/* global QUnit */

sap.ui.require(["materialcreation/hss/table/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
