sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("materialcreation.hss.table.controller.App", {
        onInit() {
        }
      });
    }
  );
  